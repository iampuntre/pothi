import re
from nltk.tokenize import word_tokenize


def bow(text):
    # only to keep english characters as on twitter we can find non-english script
    letters_only = re.sub("[^a-zA-Z]", " ", text)
    lower_case = letters_only.lower()
    bag_of_words = lower_case.split()
    return bag_of_words
