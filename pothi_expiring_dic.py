import time
from threading import RLock
import sys
from collections import OrderedDict
import math


class Expiring(OrderedDict):
    def __init__(self, max_len, max_age_seconds):
        assert max_age_seconds >= 0
        assert max_len >= 1

        OrderedDict.__init__(self)
        self.max_len = max_len
        self.max_age = max_age_seconds
        self.lock = RLock()

        if sys.version_info >= (3, 5):
            self._safe_keys = lambda: list(self.keys())
        else:
            self._safe_keys = self.keys

    def __contains__(self, key):
        """ Return True if the dict has a key, else return False. """
        try:
            with self.lock:
                item = OrderedDict.__getitem__(self, key)
                if time.time() - item[1] < self.max_age:
                    return True
                else:
                    del self[key]
        except KeyError:
            pass
        return False

    def __getitem__(self, key, with_age=False):
        """ Return the item of the dict.
        Raises a KeyError if key is not in the map.
        """
        with self.lock:
            item = OrderedDict.__getitem__(self, key)
            item_age = time.time() - item[1]
            if item_age < self.max_age:
                if with_age:
                    return item[0], self.max_age - item_age
                else:
                    return item[0]
            else:
                del self[key]
                raise KeyError(key)

    def __setitem__(self, key, value):
        """ Set d[key] to value. """
        with self.lock:
            # check if item is present
            if self.get(key) is not None:
                # update the value
                OrderedDict.__setitem__(self, key, (value, time.time()))
            if len(self) == self.max_len:
                try:
                    self.check(remove_zero=True)
                    # checking if there is space
                    if len(self) == self.max_len:
                        return False
                except KeyError:
                    pass
            OrderedDict.__setitem__(self, key, (value, time.time()))

    def get(self, key, default=None, with_age=False):
        " Return the value for key if key is in the dictionary, else default. "
        try:
            return self.__getitem__(key, with_age)
        except KeyError:
            if with_age:
                return default, None
            else:
                return default

    def check(self, remove_zero=False):
        # check if any value has to be removed and remove it
        with self.lock:
            for key in self._safe_keys():
                try:
                    item = OrderedDict.__getitem__(self, key)
                    key_ttl = self.max_age - (time.time() - item[1])
                    if item[0] >= 0 and key_ttl > 0:
                        pass
                    elif item[0] > 0 and key_ttl < 0:
                        total_seconds_remaining = item[0] * self.max_age + key_ttl
                        if total_seconds_remaining > 0:
                            value = int(math.ceil(total_seconds_remaining / self.max_age) - 1)
                            if remove_zero and value == 0:
                                del self[key]

                            new_time = time.time() - (self.max_age - total_seconds_remaining % self.max_age)
                            OrderedDict.__setitem__(self, key, (value, new_time))

                        else:
                            del self[key]
                    else:
                        del self[key]
                except:
                    pass

    def items(self):
        # check if any item needs to be removed and then print
        r = []
        self.check()
        with self.lock:
            for key in self._safe_keys():
                try:
                    item = OrderedDict.__getitem__(self, key)
                    key_ttl = self.max_age - (time.time() - item[1])
                    if self[key] is not None and self[key] > 1:
                        r.append((key, self[key]))
                except KeyError:
                    pass
            return r


