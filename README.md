## Pothi.com Task
This is my submission for pothi.com task.

### Installation
In order to install this project, first clone this repo and create a
virtual environment.
```
git clone https://iampuntre@bitbucket.org/iampuntre/pothi.git
cd pothi
virtualenv env
```

Once you have created the virtual environment, you need to install the
python packages. Type the following in order to install them.
```
source env/bin/activate
pip install -r requirements.txt
```
And you're done!

### Execution
In order to run this program, execute the following command -
```
python twitter-streaming.py --keyword MUNTOT
```

The output will be a list of tuples with the word and its count in cache.
Replace `MUNTOT` with any other keyword.


You can get in touch with me by email - nihalnayak [@] gmail [dot] com.
