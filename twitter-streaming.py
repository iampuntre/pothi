# Import the necessary methods from tweepy library
from tweepy.streaming import StreamListener
from tweepy import OAuthHandler
from tweepy import Stream
from textprocessing import bow
from pothi_expiring_dic import Expiring
import json
import time
import argparse

# Variables that contains the user credentials to access Twitter API
access_token = "77986695-MYT3r7DeYhPMuqz1Awb67uPYmsNYrzjNCi4J1q5pG"
access_token_secret = "og5k6xWNZKB1hadzoAWKMTxoc2THvWoXhit2DknBjlWR1"
consumer_key = "WOn9ZXqqXhIUIiRjKfLuv5Wws"
consumer_secret = "MED8GUsKsTejzYaMAiS32RnxsBtiVZIkNQOi9RPBAiRXe3MQrN"

# set the max_len in order to change the cache size
cache = Expiring(max_len=100, max_age_seconds=30)


class StdOutListener(StreamListener):
    def __init__(self):
        # initialize data cleaning
        pass

    def on_data(self, data):
        data = json.loads(data, encoding="utf-8")
        try:
            words = bow(data['text'])
        except:
            words = []

        for word in words:
            if cache.has_key(word):
                cache[word] = cache.get(word) + 1
            else:
                cache[word] = 1
        return True

    def on_error(self, status):
        print status


if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    parser.add_argument("--keyword", required = True)
    args = parser.parse_args()

    # This handles Twitter authetification and the connection to Twitter Streaming API
    l = StdOutListener()
    auth = OAuthHandler(consumer_key, consumer_secret)
    auth.set_access_token(access_token, access_token_secret)
    stream = Stream(auth, l)

    # This line filter Twitter Streams to capture data by the keywords
    stream.filter(track=[args.keyword], async=True)

    # prototype of the engine working
    i = 0
    while i < 5:
        i += 1
        time.sleep(60)
        print cache.items()
        print "-" * 10

    stream.disconnect()


